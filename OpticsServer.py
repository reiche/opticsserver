#!/opt/gfa/python-3.7/latest/bin/python 
 # server to check status of bunch compressor and synchronize the values 

import json
from datetime import datetime

#import time
#import queue

from threading import Timer

import logging
import logging.handlers
from logging.handlers import RotatingFileHandler

import os
import numpy as np

#  modules for interface with epics and the softIOC package
from pcaspy import Driver, SimpleServer, Alarm, Severity

import sys
import socket
#from epics import caget,caput,PV


prefix = 'S'    # Prefix'D' then it is debug mode
pf='F-OPTICSSERVER:'
pvdb = {     
        pf+'PING'      : {'type': 'string', 'scan' : 2,},
        pf+'STOP'      : {'type': 'int', 'value':0}
}
magnetPV={}

#=======================================================================
# the channels for the server
twiss = ['S','BETAX','ALPHAX','BETAY','ALPHAY','ETAX','ETAY']
data={}
for rnum in range(1,4):
    path ='/sf/data/applications/BD-OpticsServer/Settings/%2.2d-Ref.json' % rnum
    data.clear()
    with open(path,'r') as f:
        data = json.load(f)

    pvdb['%sDESCRIPTION-REF%d' % (pf,rnum)] = {'type':'char','count':1000,
                                           'value':data['description']}
    nrec = len(data['optics']['Athos']['S'])
    for tw in twiss:
        pvdb['%s%s-ATHOS-REF%d'% (pf,tw,rnum)]={'type':'float', 'prec':3, 
                                                'count': nrec, 'value':data['optics']['Athos'][tw]}
    nrec = len(data['optics']['Aramis']['S'])
    for tw in twiss:
        pvdb['%s%s-ARAMIS-REF%d'% (pf,tw,rnum)]={'type':'float', 'prec':3,
                                            'count': nrec, 'value':data['optics']['Aramis'][tw]}

    for key in data['settings'].keys():
        att = key.split(':')
        fld = att[1]
        if 'K1L' in fld or 'K2L' in fld or 'K0L' in fld:
            pvdb['%s:%s-REF%d' % (att[0][1:],fld,rnum)] = {'type':'float','prec':5,'value':data['settings'][key]}
            print('S%s:%s-REF%d' % (att[0][1:],fld,rnum),data['settings'][key])


#========================
# softIOC master class

class OpticsServer(Driver):
    def  __init__(self):

        super(OpticsServer, self).__init__()
        self.running=True

        self.version='3.0.1'
        # enable logging
        self.logfilename="/sf/data/applications/BD-OpticsServer/NewOpticsServer.log"
        handler = RotatingFileHandler(filename=self.logfilename,
                                      mode='a',
                                      maxBytes=5 * 1024 * 1024,
                                      backupCount=1,
                                      delay=0)
        logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M:%S',
                    handlers=[handler,])
    
        self.logger=logging.getLogger('OpticsServer')
        self.logger.info('Starting Server at %s' % datetime.now().strftime('%Y-%m-%d %H:%M:%S)'))
        self.logger.info('Version: %s ' % self.version)
        self.logger.info('Host: %s' % socket.gethostname())

        # do the main event loop
        self.timer_running=False
        self.time=None
        self.start()

    def start(self):
        if not self.timer_running:
            self.timer=Timer(0.2,self.run)
            self.timer.start()
            self.timer_running=True

    def run(self):
        self.setParam(pf+'PING',datetime.now().strftime('Alive at %Y-%m-%d %H:%M:%S'))

        stop=self.getParam(pf+'STOP')
        if stop > 0:
            self.logger.info('Stopping Server')
            self.timer.cancel()
            self.running=False
        else:
            self.timer_running=False
            self.start()


#
# event haendler for read and write
 
    def read(self, reason):
        value = self.getParam(reason)
        return value


    def write(self,reason,value):
        if 'STOP' in reason:
            self.setParam(reason,value)
        return
 
    def disconnect(self):
        self.logger.info('terminated at %s' % datetime.now().strftime('%Y-%m-%d %H:%M:%S)'))

    def __del__(self):
        self.disconnect()

if __name__ == '__main__':
# adding the R56 channels

    server = SimpleServer()
    server.createPV(prefix, pvdb)
    driver = OpticsServer()

    # process CA transactions
    while driver.running:
        try:
            server.process(0.1)
        except KeyboardInterrupt:
            driver.disconnect()
            print('Gracefully exiting...')
            sys.exit()





